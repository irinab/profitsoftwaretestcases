*** Settings ***
Documentation     Test cases
...
Resource          resource.robot

*** Test Cases ***
Valid Login Through Post Request
    Create Session  bolshakov   ${POST_URL}
    &{data}=   Create Dictionary   user=${USERNAME}     pass=${VALID PASSWORD}
    ${resp}=  Post Request  bolshakov  /post		data=${data}
    Should Be Equal As Strings  ${resp.status_code}  200

Invalid Login Through Post Request
    Create Session  bolshakov   ${POST_URL}
    &{data}=   Create Dictionary   user=${USERNAME}     pass=${INVALID PASSWORD}
    ${resp}=  Post Request  bolshakov  /post		data=${data}
    Should Be Equal As Strings  ${resp.status_code}  401

Invalid Login
    Given Browser displays login page
    When Email "test@bolshakov.ee" and password "fafafa" are used to log in
    Then Webmail login page is open

Valid Login
    Given Browser displays login page
    When Email "test@bolshakov.ee" and password "TestCase123" are used to log in
    Then Webmail main page is open

*** Keywords ***
#"${EMAIL}" and "${PASS}" are sent to login page
#    Create Session  bolshakov  https://bolshakov.ee:2096/login/?login_only=1
#    &{data}=   Create Dictionary   user=test@bolshakov.ee     pass=TestCase123
#    ${resp}=  Post Request  bolshakov  /post		data=${data}

#User should be logged in
#    Should Be Equal As Strings  ${resp.status_code}  200

Browser displays login page
    Open Browser To Webmail Login Page

Email "${EMAIL}" and password "${PASS}" are used to log in
    Input Username      ${EMAIL}
    Input Password      ${PASS}
    Submit Credentials
    Sleep   5s

Webmail login page is open
   Title Should Contain    Webmail Login
   Close Browser

Webmail main page is open
   Title Should Contain    Main
   Close Browser