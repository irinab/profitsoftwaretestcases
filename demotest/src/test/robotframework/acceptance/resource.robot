*** Settings ***
Documentation     A resource file.
...
Library           Selenium2Library
Library           HttpRequestLibrary

*** Variables ***
${SERVER}               bolshakov.ee:2096
${POST_URL}             https://bolshakov.ee:2096/login/?login_only=1
${BROWSER}              ff
${USERNAME}             test@bolshakov.ee
${VALID PASSWORD}       TestCase123
${INVALID PASSWORD}     invalidPasS
${LOGIN URL}            https://${SERVER}

*** Keywords ***
Open Browser To Webmail Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Webmail Login Page Should Be Open

Webmail Login Page Should Be Open
    Title Should Contain    Webmail Login

Webmail Main Page Should Be Open
    Title Should Contain    Main

Input Username
    [Arguments]             ${USERNAME}
    Input Text  name=user   ${USERNAME}

Input Password
    [Arguments]             ${PASSWORD}
    Input Text  name=pass   ${PASSWORD}

Submit Credentials
    Click Button    name=login