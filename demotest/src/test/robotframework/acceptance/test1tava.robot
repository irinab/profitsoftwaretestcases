*** Settings ***
Documentation     Test cases
...
Resource          resource.robot

*** Test Cases ***
Login Page Access
    Open Browser To Webmail Login Page
    Webmail Login Page Should Be Open
    Close Browser

Invalid Login
    Open Browser To Webmail Login Page
    Input Username      ${USERNAME}
    Input Password      ${INVALID PASSWORD}
    Submit Credentials
    Sleep   10s
    Webmail Login Page Should Be Open
    Close Browser

Valid Login
    Open Browser To Webmail Login Page
    Input Username      ${USERNAME}
    Input Password      ${VALID PASSWORD}
    Submit Credentials
    Sleep   10s
    Webmail Main Page Should Be Open
    Close Browser

